from atexit import register
from django import template

register = template.Library()

time_format = "%A, %B %d, %Y at %I:%M%p"

def mydate(oldvalue):
    return oldvalue.strftime(time_format)

register.filter('mydate', mydate)