from django.db import models

class Ticket(models.Model):
    submitter = models.CharField(max_length=100)
    title = models.CharField(max_length=200)
    body = models.CharField(max_length=1000)
    created_at = models.DateTimeField(auto_now_add=True)


    def __str__(self):
        return f"{self.title}  by {self.submitter} on {self.created_at}"