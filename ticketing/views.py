
from django.http import HttpResponse, JsonResponse
from django.shortcuts import  render
from .models import Ticket


def index(request):
    return render(request,'ticketing/home.html')

def showlayout(request):
     return render(request,'ticketing/applayout.html')

# ToDo: redirect to same page with massage
# how to check if form valid?
def submit(request):
    if request.method == "POST":
        title = request.POST.get("title")
        user = request.POST.get("username")
        body = request.POST.get("body")
        new_ticket = Ticket(title=title, submitter=user ,body=body)
        new_ticket.save()
        #return redirect("submit",{"message": "Ticket successfully submited"})
        return HttpResponse("Ticket successfully submited")
    return render(request,'ticketing/submit.html')

def tickets(request):
    tickets = Ticket.objects.all()
    return render(request,'ticketing/tickets.html',{"tickets": tickets})

def tickets_raw(request):
    all_tickets = list(Ticket.objects.values())
    return JsonResponse(all_tickets, safe=False)